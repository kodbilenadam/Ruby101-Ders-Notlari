# Ruby101 - 3. Nesil Ders ½
# Değişkenler
# metin = "Ruby" #string
# sayı = 3 #integer
# dizi = [1, 'iki', :uc]
#print sayı
#puts metin
# puts dizi
# print dizi
# nil
#bos = nil
# puts bos
# puts metin.nil?
# puts bos.nil?

# Mantıksal
# dogru = [0,''," ", "false", :nil]
# yanlis = [nil, false]
#
# for d in dogru+yanlis do
#   puts !!d
# end

# Metotlar
# def selamla(deger1)
#     "Merhaba #{deger1}"
# end
#
# puts selamla("Mahmut")
# puts selamla("Patates")

# def topla(d1,d2)
#   d1 + d2
# end

# puts topla(3,5)
# puts topla("KodBilen","Adam")
#
# print topla([1,2,3],["Berkaysln1","Enthropy","Lazarus","Slithos"])

# Kapsam
#
# degisken = 10
# puts "İlk değişkenin değeri #{degisken}"
#
# def degistir(deger)
#   degisken = deger
# end
#
# degistir(20)
#
# puts "İkinci değişkenin değeri #{degisken}"
#
# degisken = degistir(20)
#
# puts "Üçüncü değişkenin değeri #{degisken}"
