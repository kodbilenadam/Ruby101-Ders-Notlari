#Sayılar

# binary = 0b10101010 # 170
# hexadecimal = 0xABCDEF # 11259375
# octal = 0123 # 83

# puts binary
# puts hexadecimal
# puts octal

# puts binary.class 


# Duyarlılık : 15
# PI = 3.14159265358979323846264338327950 # 3.141592653589793

# Yuvarlama Problemi

# 0.33 * 10 # 3.3000000000000003

# Çözüm

# require "bigdecimal"

# puts (BigDecimal("0.33")*10).to_f 

# Bilimsel Gösterim

# bilimsel = 1.0e7 
# puts bilimsel

# Rasyonel

# rasyonel = Rational(3,2)
# puts rasyonel

# Karmaşık Sayılar

#karmasik = Complex(3,5)
#puts karmasik

#puts Complex("3-2i")

# Tip Dönüşümleri
# num.to_i || Integer(num)
# num.to_f || Float(num)
# num.to_r || Rational(num)
# num.to_c || Complex(num)

# Otomatik tip dönüşümü

# Float + Integer = Float

# puts 15.0 + 2

# Float + Rational = Float

# puts 12.3 + Rational(3,4)

# Integer + Rational = Rational

# puts 12 + Rational(3,4)

# Float + Complex = Complex
# Integer + Complex = Complex
# Rational + Complex = Complex

# puts 12.3 + Complex(3,4)
# puts 12 + Complex(3,4)